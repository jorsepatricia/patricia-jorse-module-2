
void main() {
  
  
  var mtnAppAwards = appAwards( "FNB","Banking", "FNB", 2012);
  print (mtnAppAwards.year);
}

class appAwards {
  
  String appname = "Easy Equities";
  String sector = "Fintech";
  String nameOfDeveloper = "Creston";
  int year = 2019;
  appAwards(String appName, String sector, String developer, int year) {
    this.appname = appname;
    this.sector = sector;
    this.nameOfDeveloper = nameOfDeveloper;
    this.year = year;
  }

  void printData() {
    print("Name of App :${this.appname}");
    print("Sector:${this.sector}");
    print("Developer:${this.nameOfDeveloper}");
    print("Winning Year:${this.year}");
  }
}
